#include <stdlib.h>
#include <stdio.h>

int main()
{
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip ./fifa-player-stats-database.zip");
    system("awk -F ',' '$3 < 25 && $8 > 85 && $9 != \"Manchester City\" {print $2, $3, $4, $5, $8, $9}' FIFA23_official_data.csv");
    return 0;
}