# Sisop Praktikum Modul 4 2023 Bs D03


# Nomor 1

A. Download data yang akan digunakan. Membuat file bernama _storage.c_. Untuk mendownload dataset menggunakan command

```
system("kaggle datasets download -d bryanb/fifa-player-stats-database");
```

Setelah berhasil download dalam format .zip dilakukan unzip pada file tersebut menggunakan command

```
system("unzip ./fifa-player-stats-database.zip");
```

Kemudian diminta untuk melakukan analisis dengan membaca file CSV khusus bernama FIFA23_official_data.csv, lalu mencetak pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub selain Manchester City. Informasi yang dicetak nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya.  
disini kami menggunakan awk untuk melakukan filter dan mencetak hasilnya di output terminal.

```
system("awk -F ',' '$3 < 25 && $8 > 85 && $9 != \"Manchester City\" {print $2, $3, $4, $5, $8, $9}' FIFA23_official_data.csv");
```

Dockerfile dibuat untuk setup environtment dan menjalankan storage.c untuk mengeluarkan output sesuai yang diinginkan.  
Untuk mengatur base image dari docker image

```
FROM ubuntu:latest
```

Kemudian jalankan beberapa command untuk menginsal dan update beberapa package

```
RUN apt-get update && \
    apt-get install -y \
    unzip \
    wget \
    python3 \
    python3-pip \
    build-essential
```

Install kaggle agar command kaggle pada _storage.c_ dapat dijalankan

```
RUN pip3 install kaggle
```

Membuat workdirectory tempat command dijalankan

```
WORKDIR /app
```

Mengcopy _kaggle.json_ dari local directory ke root/.kaggle/ dan mengcopy _storage.c_ ke current working directory

```
COPY kaggle.json /root/.kaggle/
COPY storage.c .
```

_Note : Pastikan anda sudah memiliki kaggle.json yang didapatkan dari akun kaggle masing masing kemudian di copy ke directory tempat dockerfile dibuat_

Kemudian compile _storage.c_ lalu di execute

```
RUN gcc storage.c -o storage
CMD ["./storage"]
```

lalu build image dengan command

```
docker build -t nandavahindra/soal1 .
```

lalu coba jalankan image agar dapat mengetahui outputnya dengan command

```
docker run -it nandavahindra/soal1
```

Setelah itu lakukan push docker image ke docker hub
Lakukan docker login dengan command

```
docker login
```

kemudian masukkan username dan password agar login sukses, lalu push docker image yang terlah dibuat tadi

```
docker push nandavahindra/soal1
```

Docker image yang telah saya push untuk soal ini dapat dilihat secara public pada link berikut
https://hub.docker.com/r/nandavahindra/fifa

Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.  
buat folder dengan

```
mkdir Barcelona Napoli
```

kemudian copy semua yang ada pada current folder ke masing masing folder

```
cp * Barcelona/
cp * Napoli/
```

setelah itu jalankan docker-compose

```
docker-compose up -d
```

Maka docker dompose dengan instance sebanyak 5 akan berhasil dijalankan.


# No 2

Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
		-Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
		-Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.
Contoh:
		/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.
Case invalid untuk bypass:
		/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
		/jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
		/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt
Case valid untuk bypass:
		/jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
		/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
		/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
		/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Untuk mengujinya, maka lakukan hal-hal berikut:
	a. Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.
	b.Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.
	c.Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.
	d.Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus
	e.Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. 
Adapun format logging yang harus kamu buat adalah sebagai berikut. 
		[STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]
Dengan keterangan sebagai berikut.
		STATUS: SUCCESS atau FAILED.
		dd: 2 digit tanggal.
		MM: 2 digit bulan.
		yyyy: 4 digit tahun.
		HH: 24-hour clock hour, with a leading 0 (e.g. 22).
		mm: 2 digit menit.
		ss: 2 digit detik.
		CMD: command yang digunakan (MKDIR atau RENAME atau RMDIR atau RMFILE atau lainnya).
		DESC: keterangan action, yaitu:
		[User]-Create directory x.
		[User]-Rename from x to y.
		[User]-Remove directory x.
		[User]-Remove file x.

Contoh:
		SUCCESS::17/05/2023-19:31:56::RENAME::Oky-Rename from x to y


Penyelesaian :

#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>
#include <sys/xattr.h>
#include <sys/wait.h>
#include <pthread.h>
#define maxSize 5000
#define pathmaxSize 1024

static const char * dirpath = "/home/praktikum/prak4/no2/nanaxgerma/src_data";

void createLog(const char *status, const char *command, const char *action){
    time_t current_time = time(NULL);
    struct tm *local_time = localtime(&current_time);

    char *buf;
    buf = getlogin();

    // Open the log file in append mode
    FILE *log_file = fopen("/home/praktikum/prak4/no2/logmucatatsini.txt", "a");
    if (log_file == NULL) {
        fprintf(stderr, "Error opening log file\n");
        return;
    }

    char usraction[maxSize];
    sprintf(usraction, "%s-%s", buf, action);
    // Format the timestamp
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", local_time);

    // Write the log entry to the file
    fprintf(log_file, "%s::%s::%s::%s\n", status, timestamp, command, usraction);

    // Close the log file
    fclose(log_file);
    
}
//mendapatkan atribut dari sebuah file
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[pathmaxSize];
    sprintf(fpath, "%s/%s", dirpath,path);
    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;
    return 0;
}


//di panggil ketika user mencoba untuk menampilkan file dan direktori yang berada pada suatu direktori spesifik.

//mencoba membaca potongan demi potongan data dari suatu file

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;
    char fpath[pathmaxSize];
    sprintf(fpath, "%s%s", dirpath,path);

    char action[maxSize];
    sprintf(action, "ACCESS directory %s.", fpath);
    createLog("SUCCESS", "READDIR", action);

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(filler(buf, de->d_name, &st, 0)) break;
    }
    closedir(dp);
    return 0;
}



static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[pathmaxSize];
    sprintf(fpath, "%s%s", dirpath,path);

    char action[maxSize];
    sprintf(action, "ACCESS file %s.", fpath);
    createLog("SUCCESS", "READ", action);
   
    int fd = 0;
    int res = 0;
    (void) fi;

    fd = open(path, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode){
    int res;
    char fpath[pathmaxSize];
    sprintf(fpath, "%s%s", dirpath,path);

    char action[maxSize];
    sprintf(action, "CREATE directory %s.", fpath);

    if(strstr(fpath, "restricted")!=NULL){
        if(strstr(fpath, "bypass")!=NULL){
            createLog("SUCCESS", "MKDIR", action);
        }else{
            createLog("FAILED", "MKDIR", action);
            return 0;
        }
    } else{
        createLog("SUCCESS", "MKDIR", action);
    }
    res = mkdir(fpath, mode);
    if(res==-1) return -errno;
    return res;
}

static int xmp_rename(const char *oldpath, const char *newpath){
    int res;
    char oldfpath[pathmaxSize];
    sprintf(oldfpath, "%s%s", dirpath,oldpath);

    char newfpath[pathmaxSize];
    sprintf(newfpath, "%s%s", dirpath,newpath);

    char action[maxSize];
    sprintf(action, "RENAME from %s to %s.", oldfpath, newfpath);

    if(strstr(oldfpath, "restricted")!=NULL){
        if(strstr(oldfpath, "bypass")!=NULL || strstr(newfpath, "bypass")!=NULL){
            createLog("SUCCESS", "RENAME", action);
        }else{
            createLog("FAILED", "RENAME", action);
            return 0;
        }
    } else{
        createLog("SUCCESS", "RENAME", action);
    }

    res = rename(oldfpath, newfpath);
    if(res == -1)return -errno;
    return res;
}

static int xmp_rmdir(const char *path){
    int res;
    char fpath[pathmaxSize];
    sprintf(fpath, "%s%s", dirpath,path);

    char action[maxSize];
    sprintf(action, "REMOVE directory %s.", fpath);

    if(strstr(path, "restricted")!=NULL){
        if(strstr(path, "bypass")!=NULL){
            createLog("SUCCESS", "RMDIR", action);
        }else{
            createLog("FAILED", "RMDIR", action);
            return 0;
        }
    } else{
        createLog("SUCCESS", "RMDIR", action);
    }

    res = rmdir(fpath);
    if (res== -1)return -errno;
    return 0;
}

static int xmp_unlink(const char *path){
    int res;
    char fpath[pathmaxSize];
    sprintf(fpath, "%s%s", dirpath,path);


    char action[maxSize];
    sprintf(action, "REMOVE file %s.", fpath);

    if(strstr(path, "restricted")!=NULL){
        if(strstr(path, "bypass")!=NULL){
            createLog("SUCCESS", "UNLINK", action);
        }else{
            createLog("FAILED", "UNLINK", action);
            return 0;
        }
    } else{
        createLog("SUCCESS", "UNLINK", action);
    }

    res = unlink(fpath);
    if (res== -1)return -errno;
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .rename = xmp_rename,
};



int  main(int  argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}


Penjelasan program germa :

```
static const char * dirpath = "/home/praktikum/prak4/no2/nanaxgerma/src_data";

```
Dalam konteks ini, dirpath mengacu pada jalur direktori yang mengarah ke direktori src_data di dalam struktur direktori yang dimulai dari /home/praktikum/prak4/no2/nanaxgerma/  
```

   time_t current_time = time(NULL);

```
Mendapatkan waktu saat ini menggunakan fungsi time(NULL) yang mengembalikan jumlah detik sejak epoch (waktu referensi).
```
    struct tm *local_time = localtime(&current_time);
```
Mengonversi waktu lokal menggunakan fungsi localtime dengan parameter alamat dari waktu saat ini. Hasilnya disimpan dalam struktur struct tm *local_time
```
    char *buf;
    buf = getlogin();
```
Mendapatkan nama pengguna saat ini dengan menggunakan fungsi getlogin(). Nama pengguna ini disimpan dalam variabel buf.
```

	 FILE *log_file = fopen("/home/praktikum/prak4/no2/logmucatatsini.txt", "a");
    if (log_file == NULL) {
        fprintf(stderr, "Error opening log file\n");
        return;
    }
```
Membuka file log dalam mode "append" menggunakan fungsi fopen. File log ini memiliki jalur /home/praktikum/prak4/no2/logmucatatsini.txt. Jika pembukaan file gagal (file tidak dapat diakses atau tidak ada), pesan kesalahan akan dicetak ke stderr dan fungsi akan mengembalikan void (return).
```

    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", local_time);
```
Memformat timestamp dengan menggunakan fungsi strftime. Format timestamp yang digunakan adalah "%d/%m/%Y-%H:%M:%S", yang menghasilkan string dengan format "dd/mm/yyyy-hh:mm:ss". Hasilnya disimpan dalam array karakter timestamp.

```

    if(strstr(fpath, "restricted")!=NULL){
        if(strstr(fpath, "bypass")!=NULL){
            createLog("SUCCESS", "MKDIR", action);
        }else{
            createLog("FAILED", "MKDIR", action);
            return 0;
        }
    } else{
        createLog("SUCCESS", "MKDIR", action);
    }
    res = mkdir(fpath, mode);
    if(res==-1) return -errno;
    return res;
```
		Memeriksa apakah fpath juga mengandung substring "bypass" menggunakan fungsi strstr. Jika iya, maka penciptaan direktori dianggap berhasil dan fungsi createLog dipanggil dengan status "SUCCESS".
		Jika fpath tidak mengandung substring "bypass", penciptaan direktori dianggap gagal. Fungsi createLog dipanggil dengan status "FAILED" dan fungsi mengembalikan 0.
		Jika fpath tidak mengandung substring "restricted", penciptaan direktori dianggap berhasil dan fungsi createLog dipanggil dengan status "SUCCESS".
		Melakukan pemanggilan fungsi mkdir dengan menggunakan jalur lengkap fpath dan hak akses mode. Hasilnya disimpan dalam variabel res.
		Memeriksa apakah nilai res sama dengan -1, yang menandakan adanya kesalahan pada fungsi mkdir. Jika iya, fungsi mengembalikan nilai negatif dari errno.
		Jika tidak ada kesalahan, fungsi mengembalikan nilai res yang berarti berhasil membuat direktori.
```
	return fuse_main(argc, argv, &xmp_oper, NULL);
```
kode return fuse_main(argc, argv, &xmp_oper, NULL) digunakan untuk memanggil fungsi fuse_main yang merupakan inti dari library FUSE (Filesystem in Userspace). Fungsi ini digunakan untuk memulai proses mounting dan menjalankan filesystem yang telah diimplementasikan.



# No 3

## Define

Beberapa `define` yang penting adalah sebagai berikut :

```
#define FUSE_USE_VERSION 28
```

Menggunakan fuse lama, bukan fuse3.

```
#define PATH_MAX 4096
#define MAX_LEN 4096 //Max path length
#define MAX_FILE 256 //Max file name length
#define MAX_EXT 64 //Max extention name length
```
Panjang maksimal untuk char array penyimpan nama file, ekstensi, dan path.

```
#define PASS "pass"
```

Password yang muncul ketika fuse ingin membaca isi file.

## Hash table

Karena fuse harus mengubah nama file sebelum user bisa mengakses directory yang dimount dan harus dikembalikan seperti semula ketika diakses tanpa **secretadmirer.c**, maka digunakan hash table untuk menyimpan nama file dan folder sebelum diubah.

```
struct nlist { /* table entry: */
    struct nlist *next; /* next entry in chain */
    char *name; /* defined name */
    char *defn; /* replacement text */
};

define HASHSIZE 101
static struct nlist *hashtab[HASHSIZE]; /* pointer table */

/* hash: form hash value for string s */
unsigned hash(char *s)
{
    unsigned hashval;
    for (hashval = 0; *s != '\0'; s++)
      hashval = *s + 31 * hashval;
    return hashval % HASHSIZE;
}

/* lookup: look for s in hashtab */
struct nlist *lookup(char *s)
{
    struct nlist *np;
    for (np = hashtab[hash(s)]; np != NULL; np = np->next)
        if (strcmp(s, np->name) == 0)
          return np; /* found */
    return NULL; /* not found */
}

char *dupEntry(char *s) /* make a duplicate of s */
{
    char *p;
    p = (char *) malloc(strlen(s)+1); /* +1 for ’\0’ */
    if (p != NULL)
       strcpy(p, s);
    return p;
}

}
/* install: put (name, defn) in hashtab */
struct nlist *install(char *name, char *defn)
{
    struct nlist *np;
    unsigned hashval;
    if ((np = lookup(name)) == NULL) { /* not found */
        np = (struct nlist *) malloc(sizeof(*np));
        if (np == NULL || (np->name = dupEntry(name)) == NULL)
          return NULL;
        hashval = hash(name);
        np->next = hashtab[hashval];
        hashtab[hashval] = np;
    } else /* already there */
        free((void *) np->defn); /*free previous defn */
    if ((np->defn = strdup(defn)) == NULL)
       return NULL;
    return np;
}
```
- hash() = Fungsi untuk menghasilkan nilai hash berdasarkan key (`name`).
- lookup() = Fungsi untuk mencari struct hash berdasarkan `name`.
- dupEntry() = Fungsi pembantu untuk membuat object struct duplikat dari struct dengan key tertentu.
- install() = Fungsi untuk memasukkan entry ke dalam hash table berdasarkan `name` dengan nilai `defn`.

## detectDir

Untuk mendeteksi file yang harus diencode + didecode isinya menjadi base64, digunakan fungsi berikut yang membaca setiap directory penyusun path menggunakan `strtok`. Jika ditemukan yang huruf awalnya valid, return 1.

```
//Function to detect if file is within valid dir based on first letter.
int detectDir(char * path){
    char temp[MAX_LEN];
    char letter;
    sprintf(temp, "%s", path);
    char * token = strtok(temp, "/");
    while( token != NULL ){
        letter = tolower(*(token));
        if(letter != 'l'){
            if(letter != 'u'){
                if(letter != 't'){
                    if(letter != 'h'){
    
                    }
                    else{
                        return 1;
                    }
                }
                else{
                    return 1;
                }
            }
            else{
                return 1;
            }
        }
        else{
            return 1;
        }
        token = strtok(NULL, "/");
    }
    return 0;
}
```

## Encode dan decode untuk base64

Berikut adalah fungsi utama yang digunakan untuk encoding dan decoding string untuk base64.
```
//Function to encode base64 (wikibooks.org)

int base64encode(const void* data_buf, size_t dataLength, char* result, size_t resultSize)
{
   const char base64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
   const uint8_t *data = (const uint8_t *)data_buf;
   size_t resultIndex = 0;
   size_t x;
   uint32_t n = 0;
   int padCount = dataLength % 3;
   uint8_t n0, n1, n2, n3;

   /* increment over the length of the string, three characters at a time */
   for (x = 0; x < dataLength; x += 3) 
   {
      /* these three 8-bit (ASCII) characters become one 24-bit number */
      n = ((uint32_t)data[x]) << 16; //parenthesis needed, compiler depending on flags can do the shifting before conversion to uint32_t, resulting to 0
      
      if((x+1) < dataLength)
         n += ((uint32_t)data[x+1]) << 8;//parenthesis needed, compiler depending on flags can do the shifting before conversion to uint32_t, resulting to 0
      
      if((x+2) < dataLength)
         n += data[x+2];

      /* this 24-bit number gets separated into four 6-bit numbers */
      n0 = (uint8_t)(n >> 18) & 63;
      n1 = (uint8_t)(n >> 12) & 63;
      n2 = (uint8_t)(n >> 6) & 63;
      n3 = (uint8_t)n & 63;
            
      /*
       * if we have one byte available, then its encoding is spread
       * out over two characters
       */
      if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
      result[resultIndex++] = base64chars[n0];
      if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
      result[resultIndex++] = base64chars[n1];

      /*
       * if we have only two bytes available, then their encoding is
       * spread out over three chars
       */
      if((x+1) < dataLength)
      {
         if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
         result[resultIndex++] = base64chars[n2];
      }

      /*
       * if we have all three bytes available, then their encoding is spread
       * out over four characters
       */
      if((x+2) < dataLength)
      {
         if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
         result[resultIndex++] = base64chars[n3];
      }
   }

   /*
    * create and add padding that is required if we did not have a multiple of 3
    * number of characters available
    */
   if (padCount > 0) 
   { 
      for (; padCount < 3; padCount++) 
      { 
         if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
         result[resultIndex++] = '=';
      } 
   }
   if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
   result[resultIndex] = 0;
   return 0;   /* indicate success */
}
```

```
//base64 Decoding function (nachtimwald.com)
size_t b64_decoded_size(const char *in)
{
	size_t len;
	size_t ret;
	size_t i;

	if (in == NULL)
		return 0;

	len = strlen(in);
	ret = len / 4 * 3;

	for (i=len; i-->0; ) {
		if (in[i] == '=') {
			ret--;
		} else {
			break;
		}
	}

	return ret;
}


int b64_isvalidchar(char c)
{
	if (c >= '0' && c <= '9')
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c >= 'a' && c <= 'z')
		return 1;
	if (c == '+' || c == '/' || c == '=')
		return 1;
	return 0;
}


int b64_decode(const char *in, char *out, size_t outlen)
{
    int b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

	size_t len;
	size_t i;
	size_t j;
	int    v;

	if (in == NULL || out == NULL)
		return 0;

	len = strlen(in);
	if (outlen < b64_decoded_size(in) || len % 4 != 0)
		return 0;

	for (i=0; i<len; i++) {
		if (!b64_isvalidchar(in[i])) {
			return 0;
		}
	}

	for (i=0, j=0; i<len; i+=4, j+=3) {
		v = b64invs[in[i]-43];
		v = (v << 6) | b64invs[in[i+1]-43];
		v = in[i+2]=='=' ? v << 6 : (v << 6) | b64invs[in[i+2]-43];
		v = in[i+3]=='=' ? v << 6 : (v << 6) | b64invs[in[i+3]-43];

		out[j] = (v >> 16) & 0xFF;
		if (in[i+2] != '=')
			out[j+1] = (v >> 8) & 0xFF;
		if (in[i+3] != '=')
			out[j+2] = v & 0xFF;
	}

	return 1;
}
```

## Driver function untuk encoding dan decoding isi file secara base64

Fungsi dibawah membaca isi file yang diberikan pathnya lalu melakukan encoding atau decoding secara base64. Fungsi membaca panjang file menggunakan `ftell`, jika terlalu panjang (di atas 5 kb) tidak dilakukan enkripsi agar tidak memenuhi stack (8 mb di linux). Fungsi lalu membaca seluruh isi sebagai string lalu melakukan encoding atau decoding sebelum menuliskannya kembali ke dalam file.

```
//Function to open and encode file contents into base64
void base64encode_file(char * path){

    //Get file size
    FILE * file = fopen(path, "r+");
    int l, l64;
    fseek(file, 0L, SEEK_END);

    l = ftell(file);
    rewind(file);
    //Calculate base64 result length (4*ceil(l/3))

    if(l > 5000){
        return NULL;
    }
    l64 = ((int) 4 * ceil((double) l / 3.00));
    char old[l*2], new[l64*2];

    fread(old, l, 1, file);

    old[l] = 0;

    base64encode(old, l, new, l64);
    new[l64] = 0;

    rewind(file);
    fprintf(file, "%s", new);

    fclose(file);
}

//Function to open and decode file contents into ascii

void base64decode_file(char * path){
    //Get file size
    FILE * file = fopen(path, "r");
    int l, l64;
    fseek(file, 0L, SEEK_END);
    l64 = ftell(file);
    rewind(file);
    if(l64 > 5000){
        return NULL;
    }
    char old[l64];
    fread(old, l64, 1, file);
    old[l64] = 0;

        //Calculate base64 result length (4*ceil(l/3))
    l = b64_decoded_size(old);
    char new[l+1];

    b64_decode(old, new, l);
    new[l] = 0;
    //printf("NEW = %s\n", new);
    fclose(file);
    file = fopen(path, "w");
    fprintf(file, "%s", new);

    fclose(file);
}

```

## Binary encoding dan decoding

Digunakan metode bagi 2 dan melihat sisa untuk melakukan encoding (12 -> 6 -> 3 -> 1, maka sisanya 0, 0, 1, 1 dengan binernya 1100). Sedangkan untuk decoding dilakukan perkalian 2 (1100 = 1*(2^3) + 1*(2^2) + 0*2 + 0*1).
```
//Helper functions for binary to decimal and vice versa
//Decimal -> binary (integer input, string to store binary)
void binary_encode(int in, char out[]){
    int n = 7;
    while(in > 0){
        if(in % 2){
            out[n] = '1';
        }
        else{
            out[n] = '0';
        }
        n -= 1;
        in = in >> 1;
    }
}

//Binary -> decimal
//If not 0 or 1, error not binary
unsigned int binary_decode(char in[]){
    int n = 7;
    int twoPow = 1;
    unsigned int total = 0;
    while(n >= 0){
        if(in[n] - 48 == 1){
            total += twoPow;
        }
        else if(in[n] - 48 == 0){
        }
        else{
            return -1;
        }
        n -= 1;
        twoPow = twoPow << 1;
    }
    return total;
}
```

## Name encoding

Pada fungsi di bawah dilakukan encoding nama file/directory. Fungsi akan melakukan operasi berbeda tergantung :
- Argumen `type` :
    - 1 : File
    - 0 : Directory
- Panjang nama, jika sama atau lebih pendek dari 4 karakter (tidak menghitung ekstensi) maka encoding menjadi binary (menggunakan nilai ascii karakter).

Fungsi sendiri menggunakan nama file/dir lama dan mengembalikan (melalui pointer) nama hasil encoding. Langkah-langkahnya adalah sebagai berikut :
- Jika merupakan file, digunakan `strrchr` untuk mencari posisi titik ekstensi, lalu menyimpan terpisah nama file dan ekstensinya (jika ada).
- Setelah mendapatkan nama file/dir, diukur panjangnya :
    - Jika <= 4 maka dilakukan encoding sebagai binary per huruf sesuai nilai desimal asciinya.
    - Jika > 4, maka dilakukan encoding menggunakan `tolower`/`toupper`:
        - File  : Menjadi huruf kecil semua.
        - Dir   : Menjadi huruf besar semua.
- Setelah nama baru didapatkan, program memasukkan nama baru dan nama lama ke dalam hash table dengan nama baru sebagai key dan nama lama dengan value.

```
//Name encoding function
//Path used for hashing dictionary 
//Type used for file or directory
/*
1 = File
0 = Dir
*/
void encode_name(char * old_name, char * new_name, char * parent_path, int type){
    //Stores file name
    char name[MAX_FILE] = "";
    //Stores extention including '.'
    char ext[MAX_EXT] = "";
    //Result
    char res[MAX_FILE] = "";
    //Temp new file path
    char path[1000] = "";

    if(type){
        // character to be searched
        char chr = '.';
        // Storing pointer returned by
        char *ptr = strrchr(old_name, chr);

        // getting the position of the character
        if (ptr)
        {
            //ptr-old_name = index of character
            sprintf(ext, "%s", ptr);
            strncpy(name, old_name, ptr - old_name);
            name[ptr-old_name] = 0;
            printf("%s + %s (%ld)\n", name,ext,ptr - old_name);
        }
        else{
            strcpy(name, old_name);
        }
    }
    else{
        strcpy(name, old_name);
    }
    //1. Checks for length
    int len = strlen(name);
    if(len <= 4){
        char biner[9] = "00000000";
        //Binary encoding
        for(int x = 0; x < len; x++){
            binary_encode(name[x], biner);
            biner[8] = 0;
            strcat(res, biner);
            if(x < len - 1){
                strcat(res, " ");
            }
        }
        strcat(res, ext);
        sprintf(new_name, "%s", res);
    }
    else{
        //upper/lower case encoding
        //Store data in dictionary as :
        /*
        key = Path of renamed file (lookup when restoring)
        item = name of old file (to restoring)
        */
       for(int x = 0; x < len; x++){

            if(type){
                res[x] = tolower(name[x]);
            }
            else{
                res[x] = toupper(name[x]);
            }

       }
        strcat(res, ext);
        sprintf(new_name, "%s", res);
        sprintf(path, "%s/%s", parent_path, res);
        printf("\nPATH : %s + %s\n", new_name, old_name);
        install(new_name, old_name);
    }

}
```

## Name decoding

Fungsi ini mirip dengan fungsi encoding, namun digunakan saat fuse exit untuk mengembalikan nama file/dir menjadi semula. Fungsi akan melakukan operasi berbeda tergantung :
- Argumen `type` :
    - 1 : File
    - 0 : Directory
- Panjang nama, jika panjang di bawah 36 (4*8 binary + 3 spasi + 1 tambahan, tidak menghitung ekstensi) dan hanya terdiri dari '0', '1', dan ' ', maka dianggap sebagai nama binary dan dilakukan decoding.

Fungsi sendiri menggunakan nama file/dir dan mengembalikan (melalui pointer) nama hasil encoding. Langkah-langkahnya adalah sebagai berikut :
- Jika merupakan file, digunakan `strrchr` untuk mencari posisi titik ekstensi, lalu menyimpan terpisah nama file dan ekstensinya (jika ada).
- Setelah mendapatkan nama file/dir, diukur panjangnya :
    - Jika <= 36 maka dilakukan decoding sebagai binary per set biner.
    - Jika > 4, maka dilakukan decoding dengan mencari nama original di hash table menggunakan nama sekarang sebagai key.
- Setelah nama lama didapatkan, dikembalikan melalui pointer.

## Rename_file

Fungsi dibawah memanggil fungsi `rename` untuk mengubah nama file jika diberikan path lama dan nama baru.
```
//Function to rename a file given old path and new name
void rename_file(char * old_path, char * new_name){
    int ret = 0;
    char new_path[MAX_LEN];
    char cur_path[MAX_LEN];
    //Stores temp because dirname modifies input array
    sprintf(cur_path, "%s", old_path);
    sprintf(new_path, "%s/%s", dirname(cur_path), new_name);
    printf("%s -> %s\n", old_path, new_path);
    ret = rename(old_path, new_path);
    if (ret)
        printf("Rename : %s\n", strerror(errno));
}

```

## FUSE

### Pembantu

Fuse yang digunakan adalah modifikasi implementasi fuse dari link https://www.cs.nmsu.edu/~pfeiffer/fuse-tutorial/.

Terdapat pula 3 fungsi dari contoh pada modul4, akan tetapi dimodifikasi agar path rootnya sesuai input `argv`.

 Modifikasi utama adalah mengubah setiap fungsi agar tidak menggunakan fungsi yang mencatat log (ke dalam file terpisah) dimana sebagian fungsi tersebut juga membantu mereturn errno code.

Sebelumnya, terdapat 1 struct dan 1 fungsi pembantu, yaitu `bb_state` dan `bb_fullpath` :
```
struct bb_state {
    char *rootdir;
    char *fusedir;
    int pass;
};

#define BB_DATA ((struct bb_state *) fuse_get_context()->private_data)


//  All the paths I see are relative to the root of the mounted
//  filesystem.  In order to get to the underlying filesystem, I need to
//  have the mountpoint.  I'll save it away early on in main(), and then
//  whenever I need a path for something I'll call this to construct
//  it.
static void bb_fullpath(char fpath[PATH_MAX], const char *path)
{
    strcpy(fpath, BB_DATA->rootdir);
    strncat(fpath, path, PATH_MAX); // ridiculously long paths will
				    // break here

}
```

`bb_state` digunakan untuk menyimpan informasi path rootdir, fusedir, dan pass. Struct akan dipass kepada fungsi `fuse_main` agar fungsi-fungsi fuse bisa mengakses informasi tersebut.

`bb_fullpath` digunakan untuk membuat absolute path file yang berawal dari root menuju directory yang dimount.

### Fungsi FUSE
```
static struct fuse_operations bb_oper = {
    .getattr = xmp_getattr,
    .statfs = bb_statfs,
    .mknod = bb_mknod,
    .unlink = bb_unlink,
    //For directory
    .opendir = bb_opendir,
    .readdir = xmp_readdir,
    .releasedir = bb_releasedir,
    .access = bb_access,
    //For files
    .open = bb_open,
    .read = xmp_read,
    .write = bb_write,
    .flush = bb_flush,
    .release = bb_release,
    .fsync = bb_fsync,
    .truncate = bb_truncate,
    //For "mv"
    .rename = bb_rename,
    //For "mkdir"
    .mkdir = bb_mkdir,
#ifdef HAVE_SYS_XATTR_H
    .getxattr = bb_getxattr,
#endif
    //Called when fuse mounts and umounts
    .init = bb_init,
    .destroy = bb_destroy
};
```
Berikut adalah daftar fungsi yang diimplementasikan agar command-command berikut dipastikan bekerja :
- cat
- nano
- mv
- mkdir

### Password pada bb_open

Fungsi pengecekan password dilakukan pada `bb_open` yang berfungsi membuka file, jika operasi yang membutuhkan dipanggil (seperti cat) maka terminal yang memulai fuse akan meminta input password.
```
int bb_open(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    int fd;
    char fpath[PATH_MAX];

    char pas[21];

    if(!BB_DATA->pass){
        printf("INSERT PASSWORD (Max 20 characters): ");
        scanf("%s", pas);
        if(strcmp(pas, PASS) == 0){
            BB_DATA->pass = 1;
        }
        else{
            printf("WRONG PASS!\n");
            return -1;
        }
    }
    
    bb_fullpath(fpath, path);
    
    // if the open call succeeds, my retstat is the file descriptor,
    // else it's -errno.  I'm making sure that in that case the saved
    // file descriptor is exactly -1.
    fd = open(fpath, fi->flags);
    if (fd < 0)
	retstat = -errno;
	
    fi->fh = fd;
if (retstat == -1) return -errno;
    return retstat;
}
```

### Auto rename pada mkdir dan mv
```
/** Rename a file */
// both path and newpath are fs-relative
int bb_rename(const char *path, const char *newpath)
{

    int ret = 0;
    char newpath2[PATH_MAX];
    char * newpath2mod;
    char newpathnew[PATH_MAX];

    char fpath[PATH_MAX];
    char fnewpath[PATH_MAX];

    strcpy(newpath2, newpath);
    newpath2mod = basename(newpath2);
    encode_name(newpath2mod, newpathnew, NULL, 1);
    sprintf(newpath2, "/%s", newpathnew);
    strcpy(newpath, newpath2);
    
    bb_fullpath(fpath, path);
    bb_fullpath(fnewpath, newpath);

    ret = rename(fpath, fnewpath);
    if (ret == -1) return -errno;

    return ret;
}

/** Create a directory */
int bb_mkdir(const char *path, mode_t mode)
{
    int ret = 0;
    char pathnew[PATH_MAX];
    char path2[PATH_MAX];
    char * path2mod;
    char fpath2[PATH_MAX];
    char fpath[PATH_MAX];
    strcpy(path2, path);
    path2mod = basename(path2);
    encode_name(path2mod, pathnew, NULL, 0);
    sprintf(path2, "/%s", pathnew);
    strcpy(path, path2);
    bb_fullpath(fpath, path);
    printf("MKDIR : %s || %s\n", fpath2, fpath);
    ret = mkdir(fpath, mode);

    if (ret == -1) return -errno;

    return ret;
}
```

Pada kedua fungsi, nama file/dir (directory baru pada mkdir dan nama file/folder baru pada rename) akan dilakukan encode menggunakan `encode_name` sebelum dilanjutkan ke fungsi masing-masing (`mkdir`/`rename`). 

NOTE : Akan muncul pesan bahwa mv/mkdir gagal karena nama file/folder tidak ditemukan. Tapi saat diperiksa akan ditemukan bahwa mv/mkdir berhasil dengan hasil nama yang sudah diencode.

### bb_destroy 

Fungsi yang dipanggil saat fuse exit. Algoritma yang dijalankan mirip dengan algoritma pada main, akan tetapi fungsinya adalah untuk decoding. 

Algoritma akan dijelaskan lebih lanjut pada main().

## MAIN
```
int  main(int  argc, char *argv[])
{
    umask(0);
    struct bb_state *bb_data;

    int fuse_stat;
    // bbfs doesn't do any access checking on its own (the comment
    // blocks in fuse.h mention some of the functions that need
    // accesses checked -- but note there are other functions, like
    // chown(), that also need checking!).  Since running bbfs as root
    // will therefore open Metrodome-sized holes in the system
    // security, we'll check if root is trying to mount the filesystem
    // and refuse if it is.  The somewhat smaller hole of an ordinary
    // user doing it with the allow_other flag is still there because
    // I don't want to parse the options string.
    if ((getuid() == 0) || (geteuid() == 0)) {
    	fprintf(stderr, "Running BBFS as root opens unnacceptable security holes\n");
    	return 1;
    }

    // See which version of fuse we're running
    fprintf(stderr, "Fuse library version %d.%d\n", FUSE_MAJOR_VERSION, FUSE_MINOR_VERSION);
    
    // Perform some sanity checking on the command line:  make sure
    // there are enough arguments, and that neither of the last two
    // start with a hyphen (this will break if you actually have a
    // rootpoint or mountpoint whose name starts with a hyphen, but so
    // will a zillion other programs)
    if ((argc < 2) || (argv[argc-1][0] == '-') || (argv[argc-1][0] == '-'))
	    bb_usage();

    bb_data = malloc(sizeof(struct bb_state));
    if (bb_data == NULL) {
	perror("main calloc");
    printf("LOL\n");
	abort();
    }

    //Modify root dir before fuse mount, when done on init(), fuse is mounted to root before init is called
        //Takes user data to get working dir

    char * path = argv[argc-2];
    printf("WORKING DIR : %s\n", path);
    FTS *fts;
    char *arg[] = { (char*) path, NULL };
    if (!(fts = fts_open(arg, FTS_LOGICAL | FTS_NOCHDIR | FTS_XDEV, NULL))) {
        if (errno != ENOENT) {
            printf("FAILED TO fts_open\n");
        }
        return NULL;
    }
    FTSENT *p;
    char toRename[MAX_FILE];
    char curPath[MAX_LEN] = "";

    while ((p = fts_read(fts)) != NULL) {

        strcpy(curPath, p->fts_path);
        switch (p->fts_info) {
            case FTS_DP:
                    if(strcmp(p->fts_path, path) == 0){
                        break;
                    }
                //printf("Dir : %s\n", p->fts_path);
                encode_name(p->fts_name, toRename, dirname(curPath), 0);
                rename_file(p->fts_path, toRename);
                break;
            case FTS_F:
                printf("File : %s + %s\n", p->fts_path, p->fts_name);
                encode_name(p->fts_name, toRename, dirname(curPath), 1);


                //printf("File encoding success\n");


                if(detectDir(p->fts_path)){
                    //printf("Content encoding\n");
                    base64encode_file(p->fts_path);
                    //printf("Content encoding SUCCESS\n");
                }
                rename_file(p->fts_path, toRename);
                break;
            case FTS_SL:
            case FTS_SLNONE:
                printf("Symbolic Link : %s\n", p->fts_path);
                break;
        }
    }
    fts_close(fts);

    // Pull the rootdir out of the argument list and save it in my
    // internal data
    bb_data->rootdir = realpath(argv[argc-2], NULL);
    bb_data->fusedir = realpath(argv[argc-1], NULL);
    bb_data->pass = 0;
    argv[argc-2] = argv[argc-1];
    argv[argc-1] = NULL;
    argc--;
        
    sleep(2);
    for(int x = argc; x >= 0; x--){
        printf("%d : %s\n", x, argv[x]);
    }
    // turn over control to fuse
    fprintf(stderr, "about to call fuse_main\n");
    fuse_stat = fuse_main(argc, argv, &bb_oper, bb_data);
    fprintf(stderr, "fuse_main returned %d\n", fuse_stat);
    
    return fuse_stat;
}
```

Fungsi main terbagi menjadi beberapa bagian :
- Mengecek kondisi launching seperti versi fuse dan jumlah argumen
- Menyiapkan struct `bb_data` yang akan dipassing ke `fuse_main`
- Melakukan encode directory yang akan dibuka melalui fuse.
- Mengisi `bb_data`
- Memanggil `fuse_main`

### Looping untuk navigasi directory (saat encoding di main() dan decoding di `bb_destroy`)

Algoritma ini menggunakan fts untuk melakukan traversal menyeluruh terhadap directory yang akan dibuka dalam fuse.

```
    char * path = argv[argc-2];
    printf("WORKING DIR : %s\n", path);
    FTS *fts;
    char *arg[] = { (char*) path, NULL };
    if (!(fts = fts_open(arg, FTS_LOGICAL | FTS_NOCHDIR | FTS_XDEV, NULL))) {
        if (errno != ENOENT) {
            printf("FAILED TO fts_open\n");
        }
        return NULL;
    }
    FTSENT *p;
    char toRename[MAX_FILE];
    char curPath[MAX_LEN] = "";

    while ((p = fts_read(fts)) != NULL) {

        strcpy(curPath, p->fts_path);
        switch (p->fts_info) {
            case FTS_DP:
                    if(strcmp(p->fts_path, path) == 0){
                        break;
                    }
                //printf("Dir : %s\n", p->fts_path);
                encode_name(p->fts_name, toRename, dirname(curPath), 0);
                rename_file(p->fts_path, toRename);
                break;
            case FTS_F:
                printf("File : %s + %s\n", p->fts_path, p->fts_name);
                encode_name(p->fts_name, toRename, dirname(curPath), 1);


                //printf("File encoding success\n");


                if(detectDir(p->fts_path)){
                    //printf("Content encoding\n");
                    base64encode_file(p->fts_path);
                    //printf("Content encoding SUCCESS\n");
                }
                rename_file(p->fts_path, toRename);
                break;
            case FTS_SL:
            case FTS_SLNONE:
                printf("Symbolic Link : %s\n", p->fts_path);
                break;
        }
    }
    fts_close(fts);
```

Setelah fts diinisialisasi, akan dilakukan navigasi terhadap semua directory dan file di dalam directory yang akan dimount. Jika merupakan directory, maka akan dilakukan encoding dan rename (menggunakan fungsi `encode_name` dan `rename_file`). Jika merupakan file, maka akan dilakukan encoding nama + jika `detectDir` mendeteksi bahwa file di dalam directory dengan nama sesuai, dilakukan encoding base64.

Hal serupa terjadi pada `bb_destoy`, namun memanggil fungsi untuk decode.

## Masalah yang dihadapi
- Diawal bingung tentang nonempty fuse (untuk fuse, bukan fuse3)
- Bingung cara kerja fuse init + destroy
- Bingung untuk command yang diminta, fungsi fuse yang dipanggil apa saja (solusi dari log implementasi fuse yang jadi acuan)
- Cara menyimpan nama lama
- Cara bikin container, karena nama dan isi file harus diubah sebelum user mulai akses fuse, maka container akan memiliki copy directory yang sudah berubah. Tidak bisa 2 container yang berjalan bersamaan memiliki directory yang belum dan sudah berubah. (Kemungkinan solusi, jika isi dan nama file tidak perlu benar-benar diubah, maka pada fungsi seperti `read` dan `getattr` bisa dilakukan encoding nama/isi sesungguhnya sebelum ditampilkan ke user.)