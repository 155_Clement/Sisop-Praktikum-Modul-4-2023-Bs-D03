#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>
#include <sys/xattr.h>
#include <sys/wait.h>
#include <pthread.h>
#define maxSize 5000
#define pathmaxSize 1024

static const char * dirpath = "/home/wildan/sisop/soal2/nanaxgerma/src_data";

void createLog(const char *status, const char *command, const char *action){
    time_t current_time = time(NULL);
    struct tm *local_time = localtime(&current_time);

    char *buf;
    buf = getlogin();

    // Open the log file in append mode
    FILE *log_file = fopen("/home/wildan/sisop/logmucatatsini.txt", "a");
    if (log_file == NULL) {
        fprintf(stderr, "Error opening log file\n");
        return;
    }

    char usraction[maxSize];
    sprintf(usraction, "%s-%s", buf, action);
    // Format the timestamp
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", local_time);

    // Write the log entry to the file
    fprintf(log_file, "%s::%s::%s::%s\n", status, timestamp, command, usraction);

    // Close the log file
    fclose(log_file);
    
}

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[pathmaxSize];
    sprintf(fpath, "%s/%s", dirpath,path);
    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;
    return 0;
}



static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;
    char fpath[pathmaxSize];
    sprintf(fpath, "%s%s", dirpath,path);

    char action[maxSize];
    sprintf(action, "ACCESS directory %s.", fpath);
    createLog("SUCCESS", "READDIR", action);

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(filler(buf, de->d_name, &st, 0)) break;
    }
    closedir(dp);
    return 0;
}



static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[pathmaxSize];
    sprintf(fpath, "%s%s", dirpath,path);

    char action[maxSize];
    sprintf(action, "ACCESS file %s.", fpath);
    createLog("SUCCESS", "READ", action);
   
    int fd = 0;
    int res = 0;
    (void) fi;

    fd = open(path, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode){
    int res;
    char fpath[pathmaxSize];
    sprintf(fpath, "%s%s", dirpath,path);

    char action[maxSize];
    sprintf(action, "CREATE directory %s.", fpath);

    if(strstr(fpath, "restricted")!=NULL){
        if(strstr(fpath, "bypass")!=NULL){
            createLog("SUCCESS", "MKDIR", action);
        }else{
            createLog("FAILED", "MKDIR", action);
            return 0;
        }
    } else{
        createLog("SUCCESS", "MKDIR", action);
    }
    res = mkdir(fpath, mode);
    if(res==-1) return -errno;
    return res;
}

static int xmp_rename(const char *oldpath, const char *newpath){
    int res;
    char oldfpath[pathmaxSize];
    sprintf(oldfpath, "%s%s", dirpath,oldpath);

    char newfpath[pathmaxSize];
    sprintf(newfpath, "%s%s", dirpath,newpath);

    char action[maxSize];
    sprintf(action, "RENAME from %s to %s.", oldfpath, newfpath);

    if(strstr(oldfpath, "restricted")!=NULL){
        if(strstr(oldfpath, "bypass")!=NULL || strstr(newfpath, "bypass")!=NULL){
            createLog("SUCCESS", "RENAME", action);
        }else{
            createLog("FAILED", "RENAME", action);
            return 0;
        }
    } else{
        createLog("SUCCESS", "RENAME", action);
    }

    res = rename(oldfpath, newfpath);
    if(res == -1)return -errno;
    return res;
}

static int xmp_rmdir(const char *path){
    int res;
    char fpath[pathmaxSize];
    sprintf(fpath, "%s%s", dirpath,path);

    char action[maxSize];
    sprintf(action, "REMOVE directory %s.", fpath);

    if(strstr(path, "restricted")!=NULL){
        if(strstr(path, "bypass")!=NULL){
            createLog("SUCCESS", "RMDIR", action);
        }else{
            createLog("FAILED", "RMDIR", action);
            return 0;
        }
    } else{
        createLog("SUCCESS", "RMDIR", action);
    }

    res = rmdir(fpath);
    if (res== -1)return -errno;
    return 0;
}

static int xmp_unlink(const char *path){
    int res;
    char fpath[pathmaxSize];
    sprintf(fpath, "%s%s", dirpath,path);


    char action[maxSize];
    sprintf(action, "REMOVE file %s.", fpath);

    if(strstr(path, "restricted")!=NULL){
        if(strstr(path, "bypass")!=NULL){
            createLog("SUCCESS", "UNLINK", action);
        }else{
            createLog("FAILED", "UNLINK", action);
            return 0;
        }
    } else{
        createLog("SUCCESS", "UNLINK", action);
    }

    res = unlink(fpath);
    if (res== -1)return -errno;
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .rename = xmp_rename,
};



int  main(int  argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}

// gcc -Wall `pkg-config fuse --cflags` germa.c -o germa `pkg-config fuse --libs`
// ./germa nanaxgerma
