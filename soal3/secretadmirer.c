#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/time.h>
#include <sys/types.h>

#define PATH_MAX 4096

#ifdef HAVE_SYS_XATTR_H
#include <sys/xattr.h>
#endif

//START OF MISC FUNCTIONS
#include <math.h>
#include <errno.h>
#include <string.h>
#include <fts.h> //For navigating directory
#include <stdio.h>
#include <ctype.h> //For tolower()
#include <inttypes.h> //For base64
#include <libgen.h> //dirname
#include <stdlib.h> //For realpath()


//gcc dir_traverse.c -lm -o dir_traverse

#define MAX_LEN 4096 //Max path length
#define MAX_FILE 256 //Max file name length
#define MAX_EXT 64 //Max extention name length

//Pasword
#define PASS "pass"
//Hashing algo, to store original file name using path as hashing
struct nlist { /* table entry: */
    struct nlist *next; /* next entry in chain */
    char *name; /* defined name */
    char *defn; /* replacement text */
};

#define HASHSIZE 101
static struct nlist *hashtab[HASHSIZE]; /* pointer table */

/* hash: form hash value for string s */
unsigned hash(char *s)
{
    unsigned hashval;
    for (hashval = 0; *s != '\0'; s++)
      hashval = *s + 31 * hashval;
    return hashval % HASHSIZE;
}

/* lookup: look for s in hashtab */
struct nlist *lookup(char *s)
{
    struct nlist *np;
    for (np = hashtab[hash(s)]; np != NULL; np = np->next)
        if (strcmp(s, np->name) == 0)
          return np; /* found */
    return NULL; /* not found */
}

char *dupEntry(char *s) /* make a duplicate of s */
{
    char *p;
    p = (char *) malloc(strlen(s)+1); /* +1 for ’\0’ */
    if (p != NULL)
       strcpy(p, s);
    return p;
}
/* install: put (name, defn) in hashtab */
struct nlist *install(char *name, char *defn)
{
    struct nlist *np;
    unsigned hashval;
    if ((np = lookup(name)) == NULL) { /* not found */
        np = (struct nlist *) malloc(sizeof(*np));
        if (np == NULL || (np->name = dupEntry(name)) == NULL)
          return NULL;
        hashval = hash(name);
        np->next = hashtab[hashval];
        hashtab[hashval] = np;
    } else /* already there */
        free((void *) np->defn); /*free previous defn */
    if ((np->defn = strdup(defn)) == NULL)
       return NULL;
    return np;
}


//Function to detect if file is within valid dir based on first letter.
int detectDir(char * path){
    char temp[MAX_LEN];
    char letter;
    sprintf(temp, "%s", path);
    char * token = strtok(temp, "/");
    while( token != NULL ){
        letter = tolower(*(token));
        if(letter != 'l'){
            if(letter != 'u'){
                if(letter != 't'){
                    if(letter != 'h'){
    
                    }
                    else{
                        return 1;
                    }
                }
                else{
                    return 1;
                }
            }
            else{
                return 1;
            }
        }
        else{
            return 1;
        }
        token = strtok(NULL, "/");
    }
    return 0;
}

//Function to encode base64 (wikibooks.org)

int base64encode(const void* data_buf, size_t dataLength, char* result, size_t resultSize)
{
   const char base64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
   const uint8_t *data = (const uint8_t *)data_buf;
   size_t resultIndex = 0;
   size_t x;
   uint32_t n = 0;
   int padCount = dataLength % 3;
   uint8_t n0, n1, n2, n3;

   /* increment over the length of the string, three characters at a time */
   for (x = 0; x < dataLength; x += 3) 
   {
      /* these three 8-bit (ASCII) characters become one 24-bit number */
      n = ((uint32_t)data[x]) << 16; //parenthesis needed, compiler depending on flags can do the shifting before conversion to uint32_t, resulting to 0
      
      if((x+1) < dataLength)
         n += ((uint32_t)data[x+1]) << 8;//parenthesis needed, compiler depending on flags can do the shifting before conversion to uint32_t, resulting to 0
      
      if((x+2) < dataLength)
         n += data[x+2];

      /* this 24-bit number gets separated into four 6-bit numbers */
      n0 = (uint8_t)(n >> 18) & 63;
      n1 = (uint8_t)(n >> 12) & 63;
      n2 = (uint8_t)(n >> 6) & 63;
      n3 = (uint8_t)n & 63;
            
      /*
       * if we have one byte available, then its encoding is spread
       * out over two characters
       */
      if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
      result[resultIndex++] = base64chars[n0];
      if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
      result[resultIndex++] = base64chars[n1];

      /*
       * if we have only two bytes available, then their encoding is
       * spread out over three chars
       */
      if((x+1) < dataLength)
      {
         if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
         result[resultIndex++] = base64chars[n2];
      }

      /*
       * if we have all three bytes available, then their encoding is spread
       * out over four characters
       */
      if((x+2) < dataLength)
      {
         if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
         result[resultIndex++] = base64chars[n3];
      }
   }

   /*
    * create and add padding that is required if we did not have a multiple of 3
    * number of characters available
    */
   if (padCount > 0) 
   { 
      for (; padCount < 3; padCount++) 
      { 
         if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
         result[resultIndex++] = '=';
      } 
   }
   if(resultIndex >= resultSize) return 1;   /* indicate failure: buffer too small */
   result[resultIndex] = 0;
   return 0;   /* indicate success */
}

//base64 Decoding function (nachtimwald.com)
size_t b64_decoded_size(const char *in)
{
	size_t len;
	size_t ret;
	size_t i;

	if (in == NULL)
		return 0;

	len = strlen(in);
	ret = len / 4 * 3;

	for (i=len; i-->0; ) {
		if (in[i] == '=') {
			ret--;
		} else {
			break;
		}
	}

	return ret;
}


int b64_isvalidchar(char c)
{
	if (c >= '0' && c <= '9')
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c >= 'a' && c <= 'z')
		return 1;
	if (c == '+' || c == '/' || c == '=')
		return 1;
	return 0;
}


int b64_decode(const char *in, char *out, size_t outlen)
{
    int b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

	size_t len;
	size_t i;
	size_t j;
	int    v;

	if (in == NULL || out == NULL)
		return 0;

	len = strlen(in);
	if (outlen < b64_decoded_size(in) || len % 4 != 0)
		return 0;

	for (i=0; i<len; i++) {
		if (!b64_isvalidchar(in[i])) {
			return 0;
		}
	}

	for (i=0, j=0; i<len; i+=4, j+=3) {
		v = b64invs[in[i]-43];
		v = (v << 6) | b64invs[in[i+1]-43];
		v = in[i+2]=='=' ? v << 6 : (v << 6) | b64invs[in[i+2]-43];
		v = in[i+3]=='=' ? v << 6 : (v << 6) | b64invs[in[i+3]-43];

		out[j] = (v >> 16) & 0xFF;
		if (in[i+2] != '=')
			out[j+1] = (v >> 8) & 0xFF;
		if (in[i+3] != '=')
			out[j+2] = v & 0xFF;
	}

	return 1;
}


//Function to open and encode file contents into base64
void base64encode_file(char * path){

    //Get file size
    FILE * file = fopen(path, "r+");
    int l, l64;
    fseek(file, 0L, SEEK_END);

    l = ftell(file);
    rewind(file);
    //Calculate base64 result length (4*ceil(l/3))

    if(l > 5000){
        return NULL;
    }
    l64 = ((int) 4 * ceil((double) l / 3.00));
    char old[l*2], new[l64*2];

    fread(old, l, 1, file);

    old[l] = 0;

    base64encode(old, l, new, l64);
    new[l64] = 0;

    rewind(file);
    fprintf(file, "%s", new);

    fclose(file);
}

//Function to open and decode file contents into ascii

void base64decode_file(char * path){
    //Get file size
    FILE * file = fopen(path, "r");
    int l, l64;
    fseek(file, 0L, SEEK_END);
    l64 = ftell(file);
    rewind(file);
    if(l64 > 5000){
        return NULL;
    }
    char old[l64];
    fread(old, l64, 1, file);
    old[l64] = 0;

        //Calculate base64 result length (4*ceil(l/3))
    l = b64_decoded_size(old);
    char new[l+1];

    b64_decode(old, new, l);
    new[l] = 0;
    //printf("NEW = %s\n", new);
    fclose(file);
    file = fopen(path, "w");
    fprintf(file, "%s", new);

    fclose(file);
}


//Helper functions for binary to decimal and vice versa
//Decimal -> binary (integer input, string to store binary)
void binary_encode(int in, char out[]){
    int n = 7;
    while(in > 0){
        if(in % 2){
            out[n] = '1';
        }
        else{
            out[n] = '0';
        }
        n -= 1;
        in = in >> 1;
    }
}

//Binary -> decimal
//If not 0 or 1, error not binary
unsigned int binary_decode(char in[]){
    int n = 7;
    int twoPow = 1;
    unsigned int total = 0;
    while(n >= 0){
        if(in[n] - 48 == 1){
            total += twoPow;
        }
        else if(in[n] - 48 == 0){
        }
        else{
            return -1;
        }
        n -= 1;
        twoPow = twoPow << 1;
    }
    return total;
}

//Name encoding function
//Path used for hashing dictionary 
//Type used for file or directory
/*
1 = File
0 = Dir
*/
void encode_name(char * old_name, char * new_name, char * parent_path, int type){
    //Stores file name
    char name[MAX_FILE] = "";
    //Stores extention including '.'
    char ext[MAX_EXT] = "";
    //Result
    char res[MAX_FILE] = "";
    //Temp new file path
    char path[1000] = "";

    if(type){
        // character to be searched
        char chr = '.';
        // Storing pointer returned by
        char *ptr = strrchr(old_name, chr);

        // getting the position of the character
        if (ptr)
        {
            //ptr-old_name = index of character
            sprintf(ext, "%s", ptr);
            strncpy(name, old_name, ptr - old_name);
            name[ptr-old_name] = 0;
            printf("%s + %s (%ld)\n", name,ext,ptr - old_name);
        }
        else{
            strcpy(name, old_name);
        }
    }
    else{
        strcpy(name, old_name);
    }
    //1. Checks for length
    int len = strlen(name);
    if(len <= 4){
        char biner[9] = "00000000";
        //Binary encoding
        for(int x = 0; x < len; x++){
            binary_encode(name[x], biner);
            biner[8] = 0;
            strcat(res, biner);
            if(x < len - 1){
                strcat(res, " ");
            }
        }
        strcat(res, ext);
        sprintf(new_name, "%s", res);
    }
    else{
        //upper/lower case encoding
        //Store data in dictionary as :
        /*
        key = Path of renamed file (lookup when restoring)
        item = name of old file (to restoring)
        */
       for(int x = 0; x < len; x++){

            if(type){
                res[x] = tolower(name[x]);
            }
            else{
                res[x] = toupper(name[x]);
            }

       }
        strcat(res, ext);
        sprintf(new_name, "%s", res);
        sprintf(path, "%s/%s", parent_path, res);
        printf("\nPATH : %s + %s\n", new_name, old_name);
        install(new_name, old_name);
    }

}

//Function to return modified name to original
//Path used for dictionary key
/*
Type
1 = File
0 = Dir
*/
void decode_name(char * old_name, char * new_name, char * file_path, int type){
    //Stores file name
    char name[MAX_FILE] = "";
    //Stores extention including '.'
    char ext[MAX_EXT] = "";
    //Result
    char res[MAX_FILE] = "";
    if(type){
        // character to be searched
        char chr = '.';
        // Storing pointer returned by
        char *ptr = strrchr(old_name, chr);

        // getting the position of the character
        if (ptr)
        {
            //ptr-old_name = index of character
            sprintf(ext, "%s", ptr);
            strncpy(name, old_name, ptr - old_name);
            name[ptr-old_name] = 0;
            printf("%s + %s (%ld)\n", name,ext,ptr - old_name);
        }
        else{
            strcpy(name, old_name);
        }
        strcat(ext, "\0");
    }
    else{
        strcpy(name, old_name);
    }
    //1. Possible binary name if : Below or equal to 36 letters, no characters other than space, '0', and '1'.
    int len = strlen(name);
    int sen = 0;
    if(len <= 36){
        for(int x = 0; x < len; x++){
            if(name[x] != '0'){
                if(name[x] != '1'){
                    if(name[x] != ' '){
                        sen = 1;
                        break;
                    }
                    else{
                        continue;
                    }
                }   
                else{
                    continue;
                }
            }
            else{
                continue;
            }
        }
    }
    //2. Not binary, continue with upper/lower
    if(sen){
        printf("111111 || %s\n", file_path);
        strcpy(new_name, lookup(old_name)->defn);
        return;
    }
    //3. Binary
    else{
        int count = 0;
        printf("NAME BINARY = %s\n", name);
        char * token = strtok(name, " ");
        printf("TOKEN = %s\n", token);

        while(token != NULL){
            res[count] = binary_decode(token);
            token = strtok(NULL, " ");
            count++;
        }
        res[count] = 0;
    }
    printf("RES : %s + %s\n", res, ext);
    sprintf(new_name, "%s%s", res, ext);
}


//Function to rename a file given old path and new name
void rename_file(char * old_path, char * new_name){
    int ret = 0;
    char new_path[MAX_LEN];
    char cur_path[MAX_LEN];
    //Stores temp because dirname modifies input array
    sprintf(cur_path, "%s", old_path);
    sprintf(new_path, "%s/%s", dirname(cur_path), new_name);
    printf("%s -> %s\n", old_path, new_path);
    ret = rename(old_path, new_path);
    if (ret)
        printf("Rename : %s\n", strerror(errno));
}

//END OF MISC FUNCTIONS


struct bb_state {
    char *rootdir;
    char *fusedir;
    int pass;
};

#define BB_DATA ((struct bb_state *) fuse_get_context()->private_data)


//  All the paths I see are relative to the root of the mounted
//  filesystem.  In order to get to the underlying filesystem, I need to
//  have the mountpoint.  I'll save it away early on in main(), and then
//  whenever I need a path for something I'll call this to construct
//  it.
static void bb_fullpath(char fpath[PATH_MAX], const char *path)
{
    strcpy(fpath, BB_DATA->rootdir);
    strncat(fpath, path, PATH_MAX); // ridiculously long paths will
				    // break here

}

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{

    int res;
    char fpath[1000];

    bb_fullpath(fpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}



static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    int res = 0;

    DIR *dp;
    struct dirent *de;


    dp = (DIR *) (uintptr_t) fi->fh;

    if (dp == NULL) return -errno;

    de = readdir(dp);
    if (de == 0) {
	res = -errno;
	return res;
    }

    do {
        // struct stat st;

        // memset(&st, 0, sizeof(st));

        // st.st_ino = de->d_ino;
        // st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, NULL, 0));

        if(res!=0) break;
    }while ((de = readdir(dp)) != NULL);

    return res;
}


static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{    
    int res = 0;

    res = pread(fi->fh, buf, size, offset);

    if (res == -1) res = -errno;

    return res;
}
//Functions based of "https://www.cs.nmsu.edu/~pfeiffer/fuse-tutorial/"
/** Get file system statistics
 *
 * The 'f_frsize', 'f_favail', 'f_fsid' and 'f_flag' fields are ignored
 *
 * Replaced 'struct statfs' parameter with 'struct statvfs' in
 * version 2.5
 */
int bb_statfs(const char *path, struct statvfs *statv)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    bb_fullpath(fpath, path);
    
    // get stats for underlying filesystem
    retstat = statvfs(fpath, statv);
    
    if (retstat == -1) return -errno;
    return retstat;
}

/** Open directory
 *
 * This method should check if the open operation is permitted for
 * this  directory
 *
 * Introduced in version 2.3
 */
int bb_opendir(const char *path, struct fuse_file_info *fi)
{
    DIR *dp;
    int retstat = 0;
    char fpath[PATH_MAX];
    
    bb_fullpath(fpath, path);

    // since opendir returns a pointer, takes some custom handling of
    // return status.
    dp = opendir(fpath);
    if (dp == NULL)
	retstat =  -errno;
    
    fi->fh = (intptr_t) dp;
    if (retstat == -1) return -errno;
    return retstat;
}

/** Release directory
 *
 * Introduced in version 2.3
 */
int bb_releasedir(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    retstat = closedir((DIR *) (uintptr_t) fi->fh);
    if (retstat == -1) return -errno;
    return retstat;
}

/**
 * Check file access permissions
 *
 * This will be called for the access() system call.  If the
 * 'default_permissions' mount option is given, this method is not
 * called.
 *
 * This method is not called under Linux kernel versions 2.4.x
 *
 * Introduced in version 2.5
 */
int bb_access(const char *path, int mask)
{
    int retstat = 0;
    char fpath[PATH_MAX];
   
    bb_fullpath(fpath, path);
    
    retstat = access(fpath, mask);
    if (retstat == -1) return -errno;
    return retstat;
}

/** File open operation
 *
 * No creation, or truncation flags (O_CREAT, O_EXCL, O_TRUNC)
 * will be passed to open().  Open should check if the operation
 * is permitted for the given flags.  Optionally open may also
 * return an arbitrary filehandle in the fuse_file_info structure,
 * which will be passed to all file operations.
 *
 * Changed in version 2.2
 */
int bb_open(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    int fd;
    char fpath[PATH_MAX];

    char pas[21];

    if(!BB_DATA->pass){
        printf("INSERT PASSWORD (Max 20 characters): ");
        scanf("%s", pas);
        if(strcmp(pas, PASS) == 0){
            BB_DATA->pass = 1;
        }
        else{
            printf("WRONG PASS!\n");
            return -1;
        }
    }
    
    bb_fullpath(fpath, path);
    
    // if the open call succeeds, my retstat is the file descriptor,
    // else it's -errno.  I'm making sure that in that case the saved
    // file descriptor is exactly -1.
    fd = open(fpath, fi->flags);
    if (fd < 0)
	retstat = -errno;
	
    fi->fh = fd;
if (retstat == -1) return -errno;
    return retstat;
}

/** Write data to an open file
 *
 * Write should return exactly the number of bytes requested
 * except on error.  An exception to this is when the 'direct_io'
 * mount option is specified (see read operation).
 *
 * Changed in version 2.2
 */
// As  with read(), the documentation above is inconsistent with the
// documentation for the write() system call.
int bb_write(const char *path, const char *buf, size_t size, off_t offset,
	     struct fuse_file_info *fi)
{
    int retstat = 0;

    // Catches stream content, encodes it, then write it.
    // no need to get fpath on this one, since I work from fi->fh not the path
    retstat = pwrite(fi->fh, buf, size, offset);
    if (retstat == -1) return -errno;

    return retstat;
}

/** Possibly flush cached data
 *
 * BIG NOTE: This is not equivalent to fsync().  It's not a
 * request to sync dirty data.
 *
 * Flush is called on each close() of a file descriptor.  So if a
 * filesystem wants to return write errors in close() and the file
 * has cached dirty data, this is a good place to write back data
 * and return any errors.  Since many applications ignore close()
 * errors this is not always useful.
 *
 * NOTE: The flush() method may be called more than once for each
 * open().  This happens if more than one file descriptor refers
 * to an opened file due to dup(), dup2() or fork() calls.  It is
 * not possible to determine if a flush is final, so each flush
 * should be treated equally.  Multiple write-flush sequences are
 * relatively rare, so this shouldn't be a problem.
 *
 * Filesystems shouldn't assume that flush will always be called
 * after some writes, or that if will be called at all.
 *
 * Changed in version 2.2
 */
// this is a no-op in BBFS.  It just //logs the call and returns success
int bb_flush(const char *path, struct fuse_file_info *fi)
{
    return 0;
}

/** Release an open file
 *
 * Release is called when there are no more references to an open
 * file: all file descriptors are closed and all memory mappings
 * are unmapped.
 *
 * For every open() call there will be exactly one release() call
 * with the same flags and file descriptor.  It is possible to
 * have a file opened more than once, in which case only the last
 * release will mean, that no more reads/writes will happen on the
 * file.  The return value of release is ignored.
 *
 * Changed in version 2.2
 */
int bb_release(const char *path, struct fuse_file_info *fi)
{
    int ret = 0;
    BB_DATA->pass = 0;
    // We need to close the file.  Had we allocated any resources
    // (buffers etc) we'd need to free them here as well.
    ret = close(fi->fh);
        if (ret == -1) return -errno;

    return ret;
}

/** Synchronize file contents
 *
 * If the datasync parameter is non-zero, then only the user data
 * should be flushed, not the meta data.
 *
 * Changed in version 2.2
 */
int bb_fsync(const char *path, int datasync, struct fuse_file_info *fi)
{
    int ret = 0;
    // some unix-like systems (notably freebsd) don't have a datasync call
#ifdef HAVE_FDATASYNC
    if (datasync){
        fdatasync(fi->fh);
	    return -errno;
    }
    else
#endif	
    ret = fsync(fi->fh);
    if (ret == -1) return -errno;

    return ret;
}

/** Rename a file */
// both path and newpath are fs-relative
int bb_rename(const char *path, const char *newpath)
{

    int ret = 0;
    char newpath2[PATH_MAX];
    char * newpath2mod;
    char newpathnew[PATH_MAX];

    char fpath[PATH_MAX];
    char fnewpath[PATH_MAX];

    strcpy(newpath2, newpath);
    newpath2mod = basename(newpath2);
    encode_name(newpath2mod, newpathnew, NULL, 1);
    sprintf(newpath2, "/%s", newpathnew);
    strcpy(newpath, newpath2);
    
    bb_fullpath(fpath, path);
    bb_fullpath(fnewpath, newpath);

    ret = rename(fpath, fnewpath);
    if (ret == -1) return -errno;

    return ret;
}

/** Create a directory */
int bb_mkdir(const char *path, mode_t mode)
{
    int ret = 0;
    char pathnew[PATH_MAX];
    char path2[PATH_MAX];
    char * path2mod;
    char fpath2[PATH_MAX];
    char fpath[PATH_MAX];
    strcpy(path2, path);
    path2mod = basename(path2);
    encode_name(path2mod, pathnew, NULL, 0);
    sprintf(path2, "/%s", pathnew);
    strcpy(path, path2);
    bb_fullpath(fpath, path);
    printf("MKDIR : %s || %s\n", fpath2, fpath);
    ret = mkdir(fpath, mode);

    if (ret == -1) return -errno;

    return ret;
}

void bb_destroy()
{
    char * path = BB_DATA->rootdir;
    //printf("WORKING DIR : %s\n", path);
    FTS *fts;
    char *arg[] = { (char*) path, NULL };
    if (!(fts = fts_open(arg, FTS_LOGICAL | FTS_NOCHDIR | FTS_XDEV, NULL))) {
     if (errno != ENOENT) {
            printf("FAILED TO fts_open\n");
    
        return NULL;
        }
    }
    
    FTSENT *p;
    char toRename[MAX_FILE];
    char curPath[MAX_LEN] = "";
    char old_name[MAX_FILE];
    char old_path[MAX_LEN];

    while ((p = fts_read(fts)) != NULL) {
 
        strcpy(curPath, p->fts_path);
        switch (p->fts_info) {
            case FTS_DP:
                if(strcmp(p->fts_path, path) == 0){
                    break;
                }
                
                //printf("Dir : %s\n", p->fts_path);
                decode_name(p->fts_name, toRename, dirname(curPath), 0);
                rename_file(p->fts_path, toRename);
                break;

            case FTS_F:
                printf("File : %s + %s\n", p->fts_path, p->fts_name);
                decode_name(p->fts_name, toRename, p->fts_path  , 1);


                //printf("File encoding success\n");


                if(detectDir(p->fts_path)){
                            //printf("Content encoding\n");
                            base64decode_file(p->fts_path);
                            //printf("Content encoding SUCCESS\n");
                        }
                        rename_file(p->fts_path, toRename);
                break;
            case FTS_SL:
            case FTS_SLNONE:
                printf("Symbolic Link : %s\n", p->fts_path);
                break;
        }
    }
    fts_close(fts);
}

void *bb_init(struct fuse_conn_info *conn){

    return BB_DATA;
}
    

//Guide to use program

void bb_usage()
{
    fprintf(stderr, "usage:  bbfs [FUSE and mount options] rootDir mountPoint\n");
    abort();
}

/** Create a file node
 *
 * There is no create() operation, mknod() will be called for
 * creation of all non-directory, non-symlink nodes.
 */
// shouldn't that comment be "if" there is no.... ?
int bb_mknod(const char *path, mode_t mode, dev_t dev)
{
    int retstat;
    char fpath[PATH_MAX];
    
    bb_fullpath(fpath, path);
    
    // On Linux this could just be 'mknod(path, mode, dev)' but this
    // tries to be be more portable by honoring the quote in the Linux
    // mknod man page stating the only portable use of mknod() is to
    // make a fifo, but saying it should never actually be used for
    // that.
    if (S_ISREG(mode)) {
	retstat = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
	if (retstat >= 0)
	    retstat = close(retstat);
    } else
	if (S_ISFIFO(mode))
	    retstat = mkfifo(fpath, mode);
	else
	    retstat = mknod(fpath, mode, dev);
    
    return retstat;
}

/** Remove a file */
int bb_unlink(const char *path)
{
    char fpath[PATH_MAX];

    bb_fullpath(fpath, path);

    return unlink(fpath);
}

/** Get extended attributes */
int bb_getxattr(const char *path, const char *name, char *value, size_t size)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    bb_fullpath(fpath, path);

    retstat = lgetxattr(fpath, name, value, size);

    if (retstat == -1) return -errno;

    return retstat;
}

/** Change the size of a file */
int bb_truncate(const char *path, off_t newsize)
{
    char fpath[PATH_MAX];
    
    bb_fullpath(fpath, path);

    return truncate(fpath, newsize);
}

static struct fuse_operations bb_oper = {
    .getattr = xmp_getattr,
    .statfs = bb_statfs,
    .mknod = bb_mknod,
    .unlink = bb_unlink,
    //For directory
    .opendir = bb_opendir,
    .readdir = xmp_readdir,
    .releasedir = bb_releasedir,
    .access = bb_access,
    //For files
    .open = bb_open,
    .read = xmp_read,
    .write = bb_write,
    .flush = bb_flush,
    .release = bb_release,
    .fsync = bb_fsync,
    .truncate = bb_truncate,
    //For "mv"
    .rename = bb_rename,
    //For "mkdir"
    .mkdir = bb_mkdir,
#ifdef HAVE_SYS_XATTR_H
    .getxattr = bb_getxattr,
#endif
    //Called when fuse mounts and umounts
    .init = bb_init,
    .destroy = bb_destroy
};



int  main(int  argc, char *argv[])
{
    umask(0);
    struct bb_state *bb_data;

    int fuse_stat;
    // bbfs doesn't do any access checking on its own (the comment
    // blocks in fuse.h mention some of the functions that need
    // accesses checked -- but note there are other functions, like
    // chown(), that also need checking!).  Since running bbfs as root
    // will therefore open Metrodome-sized holes in the system
    // security, we'll check if root is trying to mount the filesystem
    // and refuse if it is.  The somewhat smaller hole of an ordinary
    // user doing it with the allow_other flag is still there because
    // I don't want to parse the options string.
    if ((getuid() == 0) || (geteuid() == 0)) {
    	fprintf(stderr, "Running BBFS as root opens unnacceptable security holes\n");
    	return 1;
    }

    // See which version of fuse we're running
    fprintf(stderr, "Fuse library version %d.%d\n", FUSE_MAJOR_VERSION, FUSE_MINOR_VERSION);
    
    // Perform some sanity checking on the command line:  make sure
    // there are enough arguments, and that neither of the last two
    // start with a hyphen (this will break if you actually have a
    // rootpoint or mountpoint whose name starts with a hyphen, but so
    // will a zillion other programs)
    if ((argc < 2) || (argv[argc-1][0] == '-') || (argv[argc-1][0] == '-'))
	    bb_usage();

    bb_data = malloc(sizeof(struct bb_state));
    if (bb_data == NULL) {
	perror("main calloc");
    printf("LOL\n");
	abort();
    }

    //Modify root dir before fuse mount, when done on init(), fuse is mounted to root before init is called
        //Takes user data to get working dir

    char * path = argv[argc-2];
    printf("WORKING DIR : %s\n", path);
    FTS *fts;
    char *arg[] = { (char*) path, NULL };
    if (!(fts = fts_open(arg, FTS_LOGICAL | FTS_NOCHDIR | FTS_XDEV, NULL))) {
        if (errno != ENOENT) {
            printf("FAILED TO fts_open\n");
        }
        return NULL;
    }
    FTSENT *p;
    char toRename[MAX_FILE];
    char curPath[MAX_LEN] = "";

    while ((p = fts_read(fts)) != NULL) {

        strcpy(curPath, p->fts_path);
        switch (p->fts_info) {
            case FTS_DP:
                    if(strcmp(p->fts_path, path) == 0){
                        break;
                    }
                //printf("Dir : %s\n", p->fts_path);
                encode_name(p->fts_name, toRename, dirname(curPath), 0);
                rename_file(p->fts_path, toRename);
                break;
            case FTS_F:
                printf("File : %s + %s\n", p->fts_path, p->fts_name);
                encode_name(p->fts_name, toRename, dirname(curPath), 1);


                //printf("File encoding success\n");


                if(detectDir(p->fts_path)){
                    //printf("Content encoding\n");
                    base64encode_file(p->fts_path);
                    //printf("Content encoding SUCCESS\n");
                }
                rename_file(p->fts_path, toRename);
                break;
            case FTS_SL:
            case FTS_SLNONE:
                printf("Symbolic Link : %s\n", p->fts_path);
                break;
        }
    }
    fts_close(fts);

    // Pull the rootdir out of the argument list and save it in my
    // internal data
    bb_data->rootdir = realpath(argv[argc-2], NULL);
    bb_data->fusedir = realpath(argv[argc-1], NULL);
    bb_data->pass = 0;
    argv[argc-2] = argv[argc-1];
    argv[argc-1] = NULL;
    argc--;
        
    sleep(2);
    for(int x = argc; x >= 0; x--){
        printf("%d : %s\n", x, argv[x]);
    }
    // turn over control to fuse
    fprintf(stderr, "about to call fuse_main\n");
    fuse_stat = fuse_main(argc, argv, &bb_oper, bb_data);
    fprintf(stderr, "fuse_main returned %d\n", fuse_stat);
    
    return fuse_stat;
}